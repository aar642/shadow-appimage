#!/bin/sh
SELF=$(readlink -f "$0")
HERE=${SELF%/*}
export PATH="$PATH:${HERE}/usr/bin/:${HERE}/usr/share/shadow-alpha/:${HERE}/usr/share/shadow-beta/:${HERE}/usr/share/shadow-beta/:${HERE}/usr/sbin/:${HERE}/bin/:${HERE}/sbin/"
export LD_LIBRARY_PATH="${HERE}/usr/lib/:${HERE}/usr/lib/i386-linux-gnu/:${HERE}/usr/lib/x86_64-linux-gnu/:${HERE}/usr/lib/x86_64-linux-gnu/pulseaudio/:${HERE}/usr/share/shadow-alpha/resources/app.asar.unpacked/release/native/:${HERE}/usr/lib/x86_64-linux-gnu/:${HERE}/usr/lib32/:${HERE}/usr/lib64/:${HERE}/lib/:${HERE}/lib/i386-linux-gnu/:${HERE}/lib/x86_64-linux-gnu/:${HERE}/lib32/:${HERE}/lib64/:$LD_LIBRARY_PATH"
EXEC="shadow-launcher"
exec "${EXEC}" "$@"
