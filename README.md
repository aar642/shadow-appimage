<span style="color:red">**[Resurrected]** Note : Since Septembre 16th 2022 Shadow is now officially distributing debian package again and may stop distributing AppImages</span>

<span style="color:orange">**[Archived]** Note: Since March 14th 2019 Blade is now officially distributing an AppImage (https://account.shadow.tech/apps), to avoid confusion we are retiring this project!</span>

# Shadow AppImage Resurrected project

[[_TOC_]]

This project builds AppImages of Shadow Client and ShadowUSB to provide support for most systems and linux distributions. The software is not designed by us, it is the property of Shadow (https://shadow.tech/), difference with official(https://shadow.tech/shadow-apps/) and repackaged(https://gitlab.com/aar642/shadow-repackaged) is that it embed more libs.

# Known Issues

[See here](https://shadow-codex.com/shadow-linux-known-issues/)

# Download

~~Nightly Alpha version: https://gitlab.com/aar642/shadow-appimage/-/jobs/artifacts/master/raw/shadowresurrected-alpha-linux-x86_64.AppImage?job=build~~ (Broken for now)

# Debug

- Log: `/var/log/shadowusb.log`
- Version: `./shadowusb-resurrected-linux-x86_64.AppImage --version`

# ShadowUSB Installation / Update procedure

## Requirements

- fuse
- wget
- systemd
- shadow alpha (.deb, .AppImage(official or repackaged)) or / and
- shadow beta (.deb, .AppImage(official or repackaged)) or / and
- shadow (.deb, .AppImage(official or repackaged))

## Installation SteamDeck (not for Arch based distributions)

```bash
systemctl --user stop shadowusb
mkdir -p ~/appimages/
mkdir -p ~/.config/systemd/user/
wget 'https://gitlab.com/aar642/shadow-appimage/-/jobs/artifacts/master/raw/shadowusb-resurrected-linux-x86_64.AppImage?job=build' -O ~/appimages/shadowusb-resurrected-linux-x86_64.AppImage
chmod a+x ~/appimages/shadowusb-resurrected-linux-x86_64.AppImage
cat << EOF | tee ~/.config/systemd/user/shadowusb.service
[Unit]
Description=Shadow USB daemon
Wants=udev.service
After=udev.service

[Service]
ExecStart=$HOME/appimages/shadowusb-resurrected-linux-x86_64.AppImage --ws --filter
StandardOutput=null
ExecReload=/bin/kill -HUP $MAINPID
Restart=on-failure

[Install]
WantedBy=graphical.target
EOF
systemctl --user daemon-reload
systemctl --user start shadowusb
systemctl --user status shadowusb
```
- It will appear as "not installed" in launcher but should work in QuickMenu
- It should be started prior opening stream : `systemctl --user start shadowusb`
- It should be stopped once done to free forwarded devices : `systemctl --user stop shadowusb`

## Installation Other distributions

Confirmed to works on EndeavourOS, Debian 11+, Ubuntu 18.04+, Manjaro, ClearLinux, Fedora 36+
**May not work on Ubuntu 22.10 as fuse may conflict with flatpak.**

```bash
sudo systemctl stop shadowusb
sudo mkdir -p /usr/share/appimages
sudo wget 'https://gitlab.com/aar642/shadow-appimage/-/jobs/artifacts/master/raw/shadowusb-resurrected-linux-x86_64.AppImage?job=build' -O /usr/share/appimages/shadowusb-resurrected-linux-x86_64.AppImage
sudo chmod a+x /usr/share/appimages/shadowusb-resurrected-linux-x86_64.AppImage
cat << EOF | sudo tee /lib/systemd/system/shadowusb.service
[Unit]
Description=Shadow USB daemon
Wants=udev.service
After=udev.service

[Service]
ExecStart=/usr/share/appimages/shadowusb-resurrected-linux-x86_64.AppImage --ws --filter
StandardOutput=null
ExecReload=/bin/kill -HUP $MAINPID
Restart=on-failure

[Install]
WantedBy=graphical.target
EOF
sudo systemctl daemon-reload
sudo systemctl start shadowusb
sudo systemctl status shadowusb
```
- It will appear as "not installed" in launcher but should work in QuickMenu
- It should be started prior opening stream : `sudo systemctl start shadowusb`
- It should be stopped once done to free forwarded devices : `sudo systemctl stop shadowusb`

## Installation Solus

```bash
sudo systemctl stop shadowusb
sudo mkdir -p /usr/share/appimages
sudo wget 'https://gitlab.com/aar642/shadow-appimage/-/jobs/artifacts/master/raw/shadowusb-resurrected-linux-x86_64.AppImage?job=build' -O /usr/share/appimages/shadowusb-resurrected-linux-x86_64.AppImage
sudo chmod a+x /usr/share/appimages/shadowusb-resurrected-linux-x86_64.AppImage
cat << EOF | sudo tee /usr/lib64/systemd/system/shadowusb.service
[Unit]
Description=Shadow USB daemon
Wants=udev.service
After=udev.service

[Service]
ExecStart=/usr/share/appimages/shadowusb-resurrected-linux-x86_64.AppImage --ws --filter
StandardOutput=null
ExecReload=/bin/kill -HUP $MAINPID
Restart=on-failure

[Install]
WantedBy=graphical.target
EOF
sudo systemctl daemon-reload
sudo systemctl start shadowusb
sudo systemctl status shadowusb
```
- It will appear as "not installed" in launcher but should work in QuickMenu
- It should be started prior opening stream : `sudo systemctl start shadowusb`
- It should be stopped once done to free forwarded devices : `sudo systemctl stop shadowusb`

## Run manually without systemD requirements

**May not work on Ubuntu 22.10 as fuse may conflict with flatpak.**

```bash
wget 'https://gitlab.com/aar642/shadow-appimage/-/jobs/artifacts/master/raw/shadowusb-resurrected-linux-x86_64.AppImage?job=build' -O shadowusb-resurrected-linux-x86_64.AppImage
chmod a+x shadowusb-resurrected-linux-x86_64.AppImage
sudo ./shadowusb-resurrected-linux-x86_64.AppImage --ws --filter >& /dev/null
```
- It will appear as "not installed" in launcher but should work in QuickMenu
- Kill it once shadow's session terminated

# Discord servers

  Find us on Shadow Official Discord servers!

- [Discord Shadow FR](https://discord.com/invite/shadowfr)
- [Discord Shadow EN](https://discord.gg/shadow)
- [Discord Shadow DE](https://discord.com/invite/ShadowDE)
- [Discord Shadow Community Projects](https://discord.gg/9HwHnHq)

# Maintainers
![Alex^#1629](https://cdn.discordapp.com/avatars/401575828590428161/36d0ac43c2cb3a72d41c51b0c8375f65.png?size=64 "Alex^#1629")
[![BuyMeCoffee][buymecoffeebadge]][buymecoffee]

# Other Projects

[Shadow Codex](https://shadow-codex.com)

# Disclaimer

This is a community project, project is not affliated to Shadow in any way.

[Shadow](https://shadow.tech) logo and embeded Linux client is property of [Shadow](http://www.shadow.tech/).

[buymecoffee]: https://www.buymeacoffee.com/latqazuzw
[buymecoffeebadge]: https://img.shields.io/badge/buy%20me%20a%20coffee-donate-yellow?style=for-the-badge
